import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleSearchTest {
    private WebDriver driver;
    private static final String cite = "https://www.google.com";
    private static final String searchWord = "Apple";
    private static Logger logger = LogManager.getLogger(GoogleSearchTest.class);

    static {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    }

    @BeforeMethod
    public void invokeBrowser() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get(cite);
    }

    @Test
    public void search() {
        WebElement searchLine = driver.findElement(By.name("q"));
        searchLine.clear();
        searchLine.sendKeys(searchWord);
        searchLine.submit();
        logger.info("Page title is: " + driver.getTitle());
        Assert.assertTrue(driver.getTitle().contains(searchWord), "Title " + driver.getTitle() + " contains word "
                + searchWord + " !\n");
        logger.info("Current url is: " + driver.getCurrentUrl());
        Assert.assertTrue(driver.getCurrentUrl().toLowerCase().contains("q=apple"));
        WebElement imagesTabTitle = driver.findElement(By.cssSelector("a.q"));
        imagesTabTitle.click();
        WebElement imagesTab = driver.findElement(By.cssSelector("#hdtb-msb-vis [role='tab']:nth-of-type(2)"));
        logger.info("Tab with name " + imagesTab.getText() + " is opened");
        Assert.assertEquals(imagesTab.getAttribute("aria-selected"), "true");
        List<WebElement> imgElements = driver.findElements(By.xpath("//img[contains(@class, 'rg_ic rg_i')]"));
        logger.info("Images page consists of: " + imgElements.size() + " elements");
        Assert.assertNotEquals(imgElements.size(), 0);
        logger.info("Tag name of elements is: " + imgElements.get(0).getTagName());
        Assert.assertEquals(driver.findElement(By.xpath("//img[contains(@class, 'rg_ic rg_i')]")).getTagName().trim(),
                "img");
    }

    @AfterMethod
    private void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}